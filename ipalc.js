//https://github.com/dropbox/dropbox-sdk-js

var Dropbox = require('dropbox');
var fs = require('fs');
var path = require('path');

var token = 'YOUR_TOKEN_HERE';
var dbx = new Dropbox({ accessToken: token});

start();

//entery point
function start() {

  if(process.argv.length < 3){
    console.log('Usage: node ipac.js <name.ipa>');
  }else {

    var projectName = process.argv[2].replace(".ipa", "");
    var ipaName = process.argv[2];

    //upload ipa
    uploadLocalFile(ipaName, projectName, function(response, err){
      if(err){
        console.log('ipa upload error: ', err);
      }else {
        //get ipa link
        getSharedLink(response.path_lower, function(response, err) {
          if(err){
            console.log('manifest sharing error: ', err);
          }else {
            //upload manifest with link
            uploadManifest(projectName, getDirectLink(response), function(response, err) {
              if(err) {
                console.log('manifest upload error: ', err);
              }else {
                //get manifest link
                getSharedLink(response.path_lower, function(response, err) {
                  if(err){
                    console.log('manifest sharing error: ', err);
                  }else {
                    //display itms-services
                    console.log('itms-services://?action=download-manifest&url=' + getDirectLink(response));
                  }
                });
              }
            });
          }
        });
      }
    });
  }
}

//create manifest and upload it
function uploadManifest(projectName, ipaLink, callback) {
  var manifest = '<?xml version="1.0" encoding="UTF-8"?>\n\
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">\n\
<plist version="1.0">\n\
<dict>\n\
	<key>items</key>\n\
	<array>\n\
		<dict>\n\
			<key>assets</key>\n\
			<array>\n\
				<dict>\n\
					<key>kind</key>\n\
					<string>software-package</string>\n\
					<key>url</key>\n\
					<string>' + ipaLink + '</string>\n\
				</dict>\n\
			</array>\n\
			<key>metadata</key>\n\
			<dict>\n\
				<key>bundle-identifier</key>\n\
				<string>ipa-link-creator.' + projectName + '</string>\n\
				<key>bundle-version</key>\n\
				<string>1.0</string>\n\
				<key>kind</key>\n\
				<string>software</string>\n\
				<key>title</key>\n\
				<string>' + projectName + '</string>\n\
			</dict>\n\
		</dict>\n\
	</array>\n\
</dict>\n\
</plist>\n';

  var p = '/ipa-link-creator/' + projectName + '/manifest.plist';
  uploadContent (manifest, p, function(response, err) {
    callback(response, err);
  });
}

//upload local file
function uploadLocalFile(filePath, projectName, callback){
  fs.readFile(path.join(process.cwd(), filePath), function (err, contents) {
    if (err) {
      callback(null, err);
    }else {
      var p = '/ipa-link-creator/' + projectName + '/' + filePath;
      uploadContent (contents, p, function(response, err) {
        callback(response, err);
      });
    }
  });
}

//upload content
function uploadContent(contents, dbxPath, callback) {
  dbx.filesUpload({ path: dbxPath, contents: contents, mode: "overwrite" })
  .then(function (response) {
    callback(response, null);
  })
  .catch(function (err) {
    callback(null, err);
  });
}

//creates public link
function getSharedLink(dbxPath, callback) {
  dbx.sharingListSharedLinks({ path: dbxPath, direct_only: true })
  .then(function (response) {
    //if link exist get it, else create it
    if(response.links.length > 0) {
      callback(response.links[0].url, null);
    }else {
      dbx.sharingCreateSharedLinkWithSettings({ path: dbxPath })
      .then(function (response) {
        callback(response.url, null);
      })
      .catch(function (err) {
        callback(null, err);
      });
    }
  })
  .catch(function (err) {
    callback(null, err);
  });
}

//convert link to direct
function getDirectLink(userLink) {
  return 'https://dl.dropboxusercontent.com/s/' + userLink.replace("https://www.dropbox.com/s/", "").replace("?dl=0", "");
}
