This utility upload .ipa files to your dropbox storage and generate manifest installation links, with which you can install the application by ios.

##Install instructions (mac os x):  
1. Install [node.js](https://nodejs.org/en/download/)  
2. Clone repository to working directory

        cd YOUR_WORKING_DIRECTORY
        git clone https://immago@bitbucket.org/immago/ipa-link-creator.git

3. Open ipalc.js and change variable 'token' to your dropbox token
First [create](https://www.dropbox.com/developers/apps/create) app. Then go to [your app list](https://www.dropbox.com/developers/apps), chose app and press 'generate token' button  
4. make alias

        sudo nano ~/.bash_profile
        add line: alias ipalc='node PATH_TO_DIR/ipalc.js'
        #example: alias ipalc='node /Users/user/bin/ipa-link-creator/ipalc.js'

##Usage:
Navigate to directory with .ipa and use cli command: 'ipalc <name.ipa>'
